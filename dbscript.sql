create schema Lot;
go

create table Lot.Automobile
(
	Id int not null identity(1,1),
	DealerId int not null,
	[Year] nvarchar(10) not null,
	Make nvarchar(10) not null,
	Model nvarchar(10) not null,
	Color nvarchar(10),
	Mileage nvarchar(10),
	Condition nvarchar(10),
	Price int not null
)

create table Lot.Dealer
(
	Id int not null identity(1,1),
	Name nvarchar(60) not null,
	[Address] nvarchar (50) not null,
	CiSt nvarchar (50) not null,
	Zip nvarchar (10) not null
)

--keys
alter table Lot.Automobile
	add constraint pk_automobile_id primary key clustered (id);
go

alter table Lot.Dealer
   add constraint pk_dealer_id primary key clustered (id);
 go

 alter table Lot.Automobile
   add constraint fk_automobile_dealerid foreign key (dealerid) references lot.dealer(id);
go
-- done keys


create procedure Lot.sp_getdealerid @n nvarchar(50), @add nvarchar(50), @ct nvarchar(50), @zp nvarchar(50)
as
begin
	select Id 
	from lot.Dealer
	where Name = @n AND
	      [Address] = @add AND
		  CiSt = @ct AND
		  Zip = @zp;
end
go1
create procedure Lot.sp_cardealerid @yr nvarchar(50), @mk nvarchar(50), @mdl nvarchar(50)
as
begin
	select DealerId 
	from lot.Automobile
	where [Year] = @yr AND
	      Make = @mk AND
		  Model = @mdl;
end
go
create procedure Lot.sp_list @yr nvarchar(50), @mk nvarchar(50), @mdl nvarchar(50)
as
begin
	select *  
	from lot.Automobile
	where [Year] = @yr AND
	      Make = @mk AND
		  Model = @mdl;
end
go

create procedure Lot.sp_dinfo @did int
as
begin
	select *  
	from lot.Dealer
	where Id = @did;
end
go

create procedure Lot.sp_lotlist @id int
as
begin
	select *  
	from lot.Automobile
	where DealerId = @id;
end
go

insert into Lot.Dealer
values ('Silver Spring Mercedes','3301 Briggs Chaney Rd','Silver Spring, Md','20904');
go

--insert into Lot.Automobile
--values (4,'2014','Ford','F150','Gray','8900','Certified', 30500);

select * from Lot.Dealer;
go

select * from Lot.Automobile;
go
