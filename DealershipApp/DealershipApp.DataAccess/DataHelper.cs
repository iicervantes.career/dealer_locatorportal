﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DealershipApp.DataAccess
{
    public class DataHelper
    {
        //save to db here
        PortalDBEntities1 db = new PortalDBEntities1();

        //dealer methods
        public int getDealerId(Dealership a)
        {
            Dealer enti = new Dealer();
            enti.Name = a.Name;
            enti.Address = a.Address;
            enti.CiSt = a.CiSt;
            enti.Zip = a.Zip;

            var id = db.sp_getdealerid(enti.Name, enti.Address, enti.CiSt, enti.Zip).First();//a stored procedure
            
            //get dealer id

            int did= id.Value;
            return did;
        }
        public bool addCarDB(Vehicle a)
        {
        Automobile enti = new Automobile();
        enti.Make = a.Make;
        enti.Model = a.Model;
        enti.Mileage = a.Mileage;
        enti.Color = a.Color;
        enti.Year = a.Year;
        enti.Condition = a.Condition;
        enti.Price = a.Price;
        enti.DealerId = a.DealerId;

        db.Automobiles.Add(enti);
        return db.SaveChanges() > 0; 
        }
        public List<Vehicle> getLot(Dealership a)
        {
            Dealer enti = new Dealer();
            enti.Name = a.Name;
            enti.Address = a.Address;
            enti.CiSt = a.CiSt;
            enti.Zip = a.Zip;

            var id = db.sp_getdealerid(enti.Name, enti.Address, enti.CiSt, enti.Zip).First();//a stored procedure
            var carlist = db.sp_lotlist(id.Value);
            List<Vehicle> Lot = new List<Vehicle>();

            foreach (var item in carlist)
            {
                Lot.Add(new Vehicle(){
                    Make = item.Make,
                    Model = item.Model,
                    Mileage = item.Mileage,
                    Color = item.Color,
                    Year = item.Year,
                    Condition = item.Condition,
                    Price = item.Price.Value,
                    DealerId = item.DealerId}
                );//add end
            }//loop

            return Lot;
        }

        //regular user methods
        public List<Vehicle> getCarList(Vehicle a)
        {
        Automobile enti = new Automobile();
        enti.Make = a.Make;
        enti.Model = a.Model;
        enti.Year = a.Year;

        //get an autolist
        var autolist = db.sp_list(enti.Year, enti.Make, enti.Model).ToList();                
        var carlist = new List<Vehicle>();

        foreach (var item in autolist)
        {
            var dealer = db.sp_dinfo(item.DealerId).ToList();
            carlist.Add(new Vehicle()
            {
                Make = item.Make,
                Model = item.Model,
                Mileage = item.Mileage,
                Color = item.Color,
                Year = item.Year,
                Condition = item.Condition,
                Price = item.Price.Value,
                DealerId = item.DealerId,
                DealerShip = new Dealership()
                {
                    Name = dealer[0].Name,
                    CiSt = dealer[0].CiSt,
                    Address = dealer[0].Address,
                    Zip = dealer[0].Zip
                }
            }
            );//add end
        }//loop

        return carlist;
        }

    }
}
