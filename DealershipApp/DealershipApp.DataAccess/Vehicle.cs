﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DealershipApp.DataAccess
{
    public class Vehicle
    {
        public string Year { get; set; }
        public string Make { get; set; }
        public string Model { get; set; }
        public string Color { get; set; }
        public string Mileage { get; set; }
        public string Condition { get; set; }
        public int Price { get; set; }
        public int DealerId { get; set; }
        public Dealership DealerShip { get; set; }
    }
}
