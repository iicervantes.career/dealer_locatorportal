﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DealershipApp.DataAccess
{
    public class Dealership
    {
        public string Name { get; set; }
        public string Address { get; set; }
        public string CiSt { get; set; }
        public string Zip { get; set; }
    }
}
