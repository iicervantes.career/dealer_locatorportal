var app = angular.module('portal',['ngRoute']);

app.config(function ($routeProvider) {
  //routes
  $routeProvider
    .when('/',{
      templateUrl: 'pages/home.html',
      controller: 'mainCtrl'
    })
    .when('/Login',{
      templateUrl: 'pages/login.html',
      controller: 'mainCtrl'
    })
    .when('/Dealer',{
      templateUrl: 'pages/addCar.html',
      controller: 'mainCtrl'
    })
  });

app.controller('mainCtrl', function($scope,$http,$q){
  google.maps.event.addDomListener(window, 'load', initialize); 
  $scope.did = "";//dealer id
  $scope.hideDealer = false;
  $scope.makes = ["Bmw", "Chevrolet","Ford","Mazda","Mercedes","Toyota"];
  $scope.models= ["Camry","CLA 250","Expedition","F150","GL450","i8","M4",
                  "M5","Mazda3","Silverado","SLS AMG","Tundra"];
  $scope.clist =[];//car list
  $scope.lot=[];//cars in lot

  $scope.getCar = function($event){
    event.preventDefault();
    $scope.visualMap = false;
    setMapOnAll(null);//removes markers
    
    var getcar = {
      "Year":$scope.hyr,
      "Make":$scope.hmk,
      "Model":$scope.hmdl,
      "Color":"",
      "Mileage":"",
      "Condition":"",
      "DealerId": ""
    };
    
    $http.post("http:/portalrest/api/home/2",getcar)
      .then(function(response){
       //response.data has car list
      console.log("Found carlist");
      console.log(response.data);
      $scope.clist = response.data;
      for(var i = 0; i< response.data.length;i++){
          codeAddress(response.data[i].DealerShip);//put marker on maps
      }
    }); //get cars from db

  };//find car

  $scope.addeal = function($event){
    event.preventDefault();

    var dealer = {
      "Name": $scope.dnme,
      "Address": $scope.adrs,
      "CiSt": $scope.CiSt,
      "Zip":$scope.zip
    };

    $http.post("../../portalrest/api/home/1",dealer)
      .then(function(response){
       //response.data has returned data
      $scope.did = response.data;
      console.log("Dealer Id is: "+$scope.did);
      alert("Dealer Login Successfull");
      }); 
  };//get dealership id

  $scope.addCar = function($event){
    event.preventDefault();
    //car js object
    var car = {
      "Year":$scope.yr,
      "Make":$scope.mk,
      "Model":$scope.mdl,
      "Color":$scope.clr,
      "Mileage":$scope.mlg,
      "Condition": $scope.cdtn,
      "Price": $scope.prc,
      "DealerId": $scope.did
    };

    console.log(car);

    $http.post("../../portalrest/api/home",car)
      .then(function(response){
       //response.data has returned data
       if(response.data){
         console.log("Successfully added");
       }
      }); 

    alert("Car added");//done adding to db
  };//add car if your a dealer

  $scope.viewLot=function($event){
    event.preventDefault();

    var dealer = {
      "Name": $scope.dnme,
      "Address": $scope.adrs,
      "CiSt": $scope.CiSt,
      "Zip":$scope.zip
    };

    $http.post("../../portalrest/api/home/3",dealer)
      .then(function(response){
       //response.data has returned data
      $scope.lot = response.data;
      console.log($scope.lot.length);
      }); 
  };//total num of cars

  //add filters here
  $scope.color = function($event){
    event.preventDefault();
    setMapOnAll(null);//clears markers

    console.log("Current carlist: "+$scope.clist);
    var nlist = [];
    for( var i = 0; i< $scope.clist.length; i++){
        if($scope.clist[i].Color == $event.target.innerText){
          nlist.push($scope.clist[i]);
        }
    };

    $scope.clist = [];
    $scope.clist = nlist;//new list
    console.log("Filtered list: "+$scope.clist);

    for( var k = 0; k< $scope.clist.length;k++){
      codeAddress($scope.clist[k].DealerShip);//put marker on maps
    }
    
  };
  $scope.OrderbyAsc = function(){
    function compare(a,b) {
    if (a.Price < b.Price)
    return -1;
    if (a.Price > b.Price)
    return 1;
    return 0;
    } 
    $scope.clist.sort(compare);
  };
  $scope.OrderbyDsc = function(){
    function compare(a,b) {
    if (a.Price > b.Price)
    return -1;
    if (a.Price < b.Price)
    return 1;
    return 0;
    } 
    $scope.clist.sort(compare);
  };
  //End filters

  $scope.showDealer = function(){
    if(($scope.eml == "user@mail.com") && ($scope.pwd == "123")){
    $scope.hideDealer =! $scope.hideDealer;
    window.location.href = 'http:/portal/#/Dealer';
    }
    else{
      alert("Wrong username/password!");
    }
  };

});//ctlr