var geocoder;
var map;
var markers = [];

function initialize() {
  geocoder = new google.maps.Geocoder();
  var latlng = new google.maps.LatLng(39.8282, -98.5795);//center of us
  var mapOptions = {
    zoom: 4,
    center: latlng
  }
  map = new google.maps.Map(document.getElementById("map"), mapOptions);
}

function codeAddress(dealer) {
  var a = dealer.Address + " " + dealer.CiSt
          + " " + dealer.Zip;
  geocoder.geocode( { 'address': a}, function(results, status) {
    if (status == google.maps.GeocoderStatus.OK) {
      map.setCenter(results[0].geometry.location);
      var marker = new google.maps.Marker({
          map: map,
          position: results[0].geometry.location});
      var infowindow = new google.maps.InfoWindow({
        content: '<p style="color:black">'+ dealer.Name+
        '<br/>'+dealer.Address + ' ' + dealer.CiSt +
        ' '+dealer.Zip
      });
      marker.addListener('click', function() {
      infowindow.open(map, marker);
      });
      markers.push(marker);
    } 
    else {
      alert("Geocode was not successful for the following reason: " + status);
    }
  });

  document.getElementById("map", "zoom:8")
}

function setMapOnAll(map) {
  for (var i = 0; i < markers.length; i++) {
    markers[i].setMap(map);
  }
}

google.maps.event.addDomListener(window, "load", initialize);

  
  

