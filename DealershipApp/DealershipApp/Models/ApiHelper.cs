﻿using DealershipApp.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DealershipApp.Models
{
    public class ApiHelper
    {
        DataHelper access = new DataHelper();
        
        //methods to send
        public bool AddCar(VehicleApi car)
        {
            //vars
            Vehicle a = new Vehicle();
            a.Year = car.Year;
            a.Make = car.Make;
            a.Model = car.Model;
            a.Mileage = car.Mileage;
            a.Color = car.Color;
            a.Condition = car.Condition;
            a.Price = car.Price;
            a.DealerId = car.DealerId;

            return access.addCarDB(a);
           //send data down to db
        }

        public int getDealId(DealershipApi dealer)
        {
            Dealership a = new Dealership();
            a.Name = dealer.Name;
            a.Address = dealer.Address;
            a.CiSt = dealer.CiSt;
            a.Zip = dealer.Zip;

            return access.getDealerId(a);

        }

        public List<VehicleApi> GetLot(DealershipApi dealer)
        {
            Dealership a = new Dealership();
            a.Name = dealer.Name;
            a.Address = dealer.Address;
            a.CiSt = dealer.CiSt;
            a.Zip = dealer.Zip;

            List<VehicleApi> lot = new List<VehicleApi>();
            foreach (var item in access.getLot(a))
            {
                lot.Add(new VehicleApi()
                {
                    Make = item.Make,
                    Model = item.Model,
                    Mileage = item.Mileage,
                    Color = item.Color,
                    Year = item.Year,
                    Condition = item.Condition,
                    Price = item.Price,
                    DealerId = item.DealerId
                });
            }
            return lot;
        }

        public List<VehicleApi> CarList(VehicleApi car)
        {
            Vehicle a = new Vehicle();
            a.Year = car.Year;
            a.Make = car.Make;
            a.Model = car.Model;
            a.Mileage = car.Mileage;
            a.Color = car.Color;
            a.Condition = car.Condition;
            a.Price = car.Price;
            a.DealerId = car.DealerId;

            List<VehicleApi> ret = new List<VehicleApi>();
            foreach (var item in access.getCarList(a))
            {
                ret.Add(new VehicleApi()
                {
                    Make = item.Make,
                    Model = item.Model,
                    Mileage = item.Mileage,
                    Color = item.Color,
                    Year = item.Year,
                    Condition = item.Condition,
                    Price = item.Price,
                    DealerId = item.DealerId,
                    DealerShip = new DealershipApi()
                    {
                        Name = item.DealerShip.Name,
                        Address = item.DealerShip.Address,
                        CiSt = item.DealerShip.CiSt,
                        Zip = item.DealerShip.Zip
                    }
                });
            }
            return ret;
        }

    }
}