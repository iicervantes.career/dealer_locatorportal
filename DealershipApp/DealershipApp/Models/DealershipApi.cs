﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DealershipApp.Models
{
    public class DealershipApi
    {
        public string Name { get; set; }
        public string Address { get; set; }
        public string CiSt { get; set; }
        public string Zip { get; set; }
    }
}