﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DealershipApp.Models
{
    public class VehicleApi
    {
        public string Year { get; set; }
        public string Make { get; set; }
        public string Model { get; set; }
        public string Color { get; set; }
        public string Mileage { get; set; }
        public string Condition { get; set; }
        public int Price { get; set; }
        public int DealerId { get; set; }
        public DealershipApi DealerShip { get; set; }
    }
}