﻿using DealershipApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace DealershipApp.Controllers
{
    public class HomeController : ApiController
    {
        ApiHelper helper = new ApiHelper();

        //add car to dealer
        [HttpPost]
        public bool AddCar([FromBody] VehicleApi car)
        {
            //grab vehicle location
            if (helper.AddCar(car))
            {
                return true;
            }
            else
                return false;

        }

        //get dealerid
        [Route("api/home/1")]
        [HttpPost]
        public int GetDealer([FromBody] DealershipApi dealer)
        {
            int i = helper.getDealId(dealer);

            return i;
        }

        [Route("api/home/3")]
        [HttpPost]
        public List<VehicleApi> DealerLot([FromBody] DealershipApi dealer)
        {
            if (dealer != null)
            {
                return helper.GetLot(dealer);
            }
            else
                return new List<VehicleApi>();
        }



        //find car form, get dealer info
        [Route("api/home/2")]
        [HttpPost]
        public List<VehicleApi> GetCarList([FromBody] VehicleApi car)
        {
            //send car info
            //returns car list
            if (car != null)
            {
                return helper.CarList(car);
            }
            else
                return new List<VehicleApi>();
              
        }

    }
}
